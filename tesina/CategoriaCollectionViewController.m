//
//  CategoriaCollectionViewController.m
//  tesina
//
//  Created by Nancy on 03/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import "CategoriaCollectionViewController.h"

@interface CategoriaCollectionViewController ()

@end

@implementation CategoriaCollectionViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"Usuario Seleccionado:%@",  self.usuarioSeleccionado);
    
     [self recuperarNombres];
    

  
}


//metodo que se realiza cuando aparece en escena
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self recuperarNombres];
    [self.collectionView reloadData];
}


-(void) recuperarNombres{
    
    DBManager *dbManager = [[DBManager alloc] init];
    
    NSMutableArray *matriz = [dbManager obtenerConsulta:@"Select id,nombre from categoria"];
    
    self.imagenesObtenidas=matriz;
    
    int numeroRegistros = (int)matriz.count;
    
    for (int i=0; i<numeroRegistros; i++){
        
        
        
        for (int j=0; j<2; j++){
            
            NSLog(@"%@", self.imagenesObtenidas[i][j]);
            
            
        }
    }
    
    
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    //regresar el numero de items
    
    return self.imagenesObtenidas.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    //automaticamente se hace un ciclo con respecto al número de items
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];

    
    //obteniendo el string adecuado
    NSString *texto = self.imagenesObtenidas[indexPath.row][1];
    
    //parseando texto
    NSString *textoString= [NSString stringWithFormat:@"%@",texto];
    
    //obteniendo paths
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    //obteniendo path destino con el nombre de la imagen
    NSString *toPath = [NSString stringWithFormat:@"%@/categorias/%@.png",documentDirectory,textoString];
    
    
    //variable para validar si el archivo existe
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:toPath];
    
    
    if (fileExists == YES){
        
        //Buscando el label atraves del tag
        UILabel *labelName = (UILabel *)[cell viewWithTag:2];
        //BUscando la imagen a través del tag
        UIImageView *imagen = (UIImageView *)[cell viewWithTag:1];
        
        //guardando el texto
        labelName.text = textoString;
        
        //obteniendo imagen
        UIImage *imagenObtenida = [UIImage imageWithContentsOfFile:toPath];
        //asignando imagen
        imagen.image = imagenObtenida;
        
        
    }
    else {
    
        NSLog(@"Error");
    }
    
    
   
    
    
    
    return cell;
}



-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if( ![segue.identifier isEqualToString:@"anadirCategoria"]){
    
        //obteniendo arreglo
        NSArray *elementos= [self.collectionView indexPathsForSelectedItems];
        //obteniendo valor del index del seleccionado
        NSInteger index=((NSIndexPath *)elementos[0]).row;
        
        
        //parseando el string del seleccionado
        NSMutableArray *seleccionado= self.imagenesObtenidas[index];

        
        
        if([segue.identifier isEqualToString:@"juegoSegue"]){
            
            NSLog(@"Pasar variables al juego");
            JuegoViewController *controller=(JuegoViewController *) segue.destinationViewController;
            
            
            //pasando la variable a la siguiente vista
            controller.idUsuario = self.usuarioSeleccionado[0];
            controller.idCategoria1 = seleccionado[0];
            controller.nombreCategoria1 = seleccionado[1];
            
        }
        
        
        if([segue.identifier isEqualToString:@"modificarCategoria"]){
            
            CategoriaCrudViewController *categoriaController=(CategoriaCrudViewController *) segue.destinationViewController;
            
            
            //pasando la variable a la siguiente vista
            categoriaController.categoria = seleccionado;
        }
        
        
        if([segue.identifier isEqualToString:@"categoriaMaterialSegue"]){
            
            MaterialCollectionViewController *controller=(MaterialCollectionViewController *) segue.destinationViewController;
            
            
            //pasando la variable a la siguiente vista
            controller.idCategoria = seleccionado[0];
        }

        
      

    }
    
}


@end
