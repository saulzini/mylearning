//
//  JuegoViewController.m
//  tesina
//
//  Created by Nancy on 13/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import "JuegoViewController.h"

@interface JuegoViewController ()

@end

@implementation JuegoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    [self recuperarCategorias];
    [self botonesCategorias];
    
    [self recuperarMaterial];
    
    
    NSLog(@"id Usuario: %@", self.idUsuario);
    NSLog(@"id Categoria: %@", self.idCategoria1);
   
    self.sounds = [[NSMutableArray alloc] initWithObjects:@"correct",@"wrong", nil];
    
    self.BotonesCategoria1=[[NSMutableArray alloc] init];
    self.BotonesCategoria2=[[NSMutableArray alloc] init];
    
    [self startCounter];
    self.imgLabelClasificadas.text= [NSString stringWithFormat:@"Imágenes:%d/%d",self.imagenesClasificadas,self.totalElementos];
    
    
    
    
    
   
}

-(void) botonesCategorias{

    UIButton *categoria1= (UIButton *)self.CategoryButtonsCollection[0];
    UIButton *categoria2= (UIButton *)self.CategoryButtonsCollection[1];
    

    
    //obteniendo paths
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    
    //obteniendo path destino con el nombre de la imagen
    NSString *toPath = [NSString stringWithFormat:@"%@/categorias/%@.png",documentDirectory,
                        self.nombreCategoria1];
    //obteniendo path destino con el nombre de la imagen
    NSString *toPath2 = [NSString stringWithFormat:@"%@/categorias/%@.png",documentDirectory,
                        self.categoriasObtenidas[0][1]];
    
    //obteniendo imagen
    UIImage *imagenObtenida = [UIImage imageWithContentsOfFile:toPath];
    //obteniendo imagen
    UIImage *imagenObtenida2 = [UIImage imageWithContentsOfFile:toPath2];
    
    
    [categoria1 setBackgroundImage:imagenObtenida forState:UIControlStateNormal];
    [categoria1 setTitle:self.nombreCategoria1 forState:UIControlStateNormal];
    
    [categoria2 setBackgroundImage:imagenObtenida2 forState:UIControlStateNormal];
    [categoria2 setTitle:self.categoriasObtenidas[0][1] forState:UIControlStateNormal];


}


-(void) recuperarMaterial{
    
    DBManager *dbManager = [[DBManager alloc] init];
    
    //categoria 1
    NSString *query = [NSString stringWithFormat:@"Select m.id,m.nombre from material m, categoria_material cm where m.id = cm.id_material and cm.id_categoria=%@",self.idCategoria1];
    

    NSMutableArray *matriz = [dbManager obtenerConsulta:query];
    
    self.imagenesObtenidas=matriz;
    
    self.numeroRegistros = (int)matriz.count;
    
    if (self.numeroRegistros >=5 ){
        self.numeroRegistros =5;
    }

    
    
    
    //categoria 2
    NSString *query2 = [NSString stringWithFormat:@"Select m.id,m.nombre from material m, categoria_material cm where m.id = cm.id_material and cm.id_categoria=%@",self.categoriasObtenidas[0][0]];
    
    
    NSMutableArray *matriz2 = [dbManager obtenerConsulta:query2];
    
    self.imagenesObtenidas2=matriz2;
    
    self.numeroRegistros2 = (int)matriz2.count;
    
    if (self.numeroRegistros2 >=5 ){
        self.numeroRegistros2 =5;
    }
    
   
    
        for (int i=0; i<self.numeroRegistros; i++){
            
            for (int j=0; j<2; j++){
                
                NSLog(@"%@", self.imagenesObtenidas[i][j]);
            }
            
            [self.BotonesCategoria1 addObject: [self CrearCartita:self.imagenesObtenidas[i][1]] ];
            
        }
        
        
        
        for (int i=0; i<self.numeroRegistros2; i++){
            
            for (int j=0; j<2; j++){
                
                NSLog(@"%@", self.imagenesObtenidas2[i][j]);
            }
            
            [self.BotonesCategoria2 addObject: [self CrearCartita:self.imagenesObtenidas2[i][1]] ];
            
        }
    
    
    self.totalElementos = self.numeroRegistros+self.numeroRegistros2;
    
    if (self.totalElementos > 10){
        self.totalElementos = 10;
    
    }
    
    
}




-(void) recuperarCategorias{
    
    DBManager *dbManager = [[DBManager alloc] init];
    
   
    NSString *query = [NSString stringWithFormat:@"Select id,nombre from categoria where id != %@ ",self.idCategoria1];
    
    
    
    NSMutableArray *matriz = [dbManager obtenerConsulta:query];
    
    
    self.categoriasObtenidas=matriz;
    
   
}



-(void) playSound{
    //extension mp3
    NSString *path= [[NSBundle mainBundle] pathForResource:self.selectedSound ofType:@"wav"];
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:nil];
    //infinito menor a 0
    self.audioPlayer.numberOfLoops=1;
    //reproduce
    [self.audioPlayer play];
    
}


-(void) startCounter{
    
      //LLamas cada segundo
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector: @selector(updateWatchLabel) userInfo:nil repeats:YES];
    
    
}



-(void) updateWatchLabel{
    
    
    
    self.labelTiempo.text= [NSString stringWithFormat:@"Tiempo:%d ",self.counter];
    
    /*
    if (self.counter>=60){
        
        
        [self.timer invalidate];
        
        int total=self.countadorRojo+self.countadorVerde;
        if (total==5){
            
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Time is over" message:@"Well done!!" delegate:self cancelButtonTitle:@"New game" otherButtonTitles: nil,nil];
            
            [alert show];
            
        }
        
        
        else {
            
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Time is over" message:@"Keep trying" delegate:self cancelButtonTitle:@"New game" otherButtonTitles: nil,nil];
            
            [alert show];
            
        }
     
        
    }
    */
    
    if (self.imagenesClasificadas >= self.totalElementos ){
    
        [self.timer invalidate];
        
        //Creando mensaje de error de nombre repetido
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Felicidades"
                                     message:@"Intenta con otra categoría :)"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        // boton de accion
        UIAlertAction* aceptar = [UIAlertAction
                                  actionWithTitle:@"Aceptar"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                      //Se cierra la ventana actual y se regresa una anterior
                                      [self.navigationController popViewControllerAnimated:YES];
                                      
                                      DBManager *dbManager = [[DBManager alloc] init];
                                      
                                     
                                      
                                      
                                      
                                      NSString *queryM = [NSString stringWithFormat: @"SELECT count(*)+1 FROM jugar"];
                                      
                                      NSLog(@"%@",queryM);
                                      
                                      NSMutableArray *matrizM = [dbManager obtenerConsulta:queryM];
                                      
                                      
                                      NSString *queryR = [NSString stringWithFormat: @"insert into jugar(id,id_categoria,id_usuario,tiempo_realizado,puntaje,imagenes_total,fecha_juego) values('%@','%@','%@',%d,%d,%d,'%@')",matrizM[0][0],self.idCategoria1,self.idUsuario,self.counter,self.imagenesClasificadas,self.totalElementos,[self getNowToString]];
                                      
                                      NSLog(@"%@",queryR);
                                      
                                      
                                      BOOL aceptado = [dbManager DML: queryR];
                                      
                                      if (aceptado){
                                          NSLog(@"Se ha guardado la relacion");
                                          //Se cierra la ventana actual y se regresa una anterior
                                          [self.navigationController popViewControllerAnimated:YES];
                                          
                                      }
                                      else {
                                          NSLog(@"Error");
                                      }
                                      

                                  }];
        
        [alert addAction:aceptar];
        [self presentViewController:alert animated:YES completion:nil];

        
        
    }
    
    self.counter++;
    
    
    
}



- (UIButton*)CrearCartita: (NSString *) nombre {
    
    
    int posX=410;
    int posY=470;
    
    
    
    //obteniendo paths
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    
    //obteniendo path destino con el nombre de la imagen
    NSString *toPath = [NSString stringWithFormat:@"%@/materiales/%@.png",documentDirectory,
                        nombre];
    
    //obteniendo imagen
    UIImage *imagenObtenida = [UIImage imageWithContentsOfFile:toPath];
    
    
    
    UIButton *newButton = [[UIButton alloc] init];
    
    [newButton setBackgroundImage:imagenObtenida forState:UIControlStateNormal];

    [newButton setTitle:nombre forState:UIControlStateNormal];
    
    
    
    [newButton setFrame:CGRectMake(posX,posY, 200, 300)];
    [self.view addSubview:newButton];
    
   
    UIPanGestureRecognizer *pgr = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(drag:)];
    [newButton addGestureRecognizer:pgr];
    
    
    return newButton;
    
   
    
    
    
}





- (void)drag:(UIPanGestureRecognizer *)recognizer {
    
    
    
    CGPoint translation = [recognizer translationInView:self.view];
    //Se obtienen los puntos de x y Y
    CGFloat newX = recognizer.view.center.x + translation.x;
    
    CGFloat newY = recognizer.view.center.y + translation.y;
    
    recognizer.view.center = CGPointMake(newX, newY);
    
    [recognizer setTranslation:CGPointMake(0,0) inView:self.view];
    
    
    
    
    UIButton *draggedButton=(UIButton *)recognizer.view;
    
    
    if(recognizer.state == UIGestureRecognizerStateEnded){
        
        for(int i=0;i<self.CategoryButtonsCollection.count;i++){
            
            UIButton *b= (UIButton *) self.CategoryButtonsCollection[i];
            
            if(CGRectIntersectsRect(recognizer.view.frame, b.frame)){
                
               // NSLog(@"Choco contra: %@",b.currentTitle);
               // NSLog(@"Yo soy: %@",draggedButton.currentTitle);
                
                
                
                //validacion de categoria 1
                if(b == ((UIButton *)self.CategoryButtonsCollection[0])) {
                    
                    BOOL encontrado = [self seEncontro:draggedButton :self.numeroRegistros:self.imagenesObtenidas];
                    
                    if (encontrado){
                       
                        [self desapacere:draggedButton];
                        self.selectedSound = self.sounds[0];
                        [self playSound];
                        self.imagenesClasificadas++;
                       
                    }
                    else {
                        
                         [self regresate:draggedButton];
                        self.selectedSound = self.sounds[1];
                        [self playSound];
                      
                    
                    }
                    
                    
                }
                //categoria 2
                else {
                    
                    BOOL encontrado = [self seEncontro:draggedButton :self.numeroRegistros2:self.imagenesObtenidas2];
                    
                    if (encontrado){
                      
                        [self desapacere:draggedButton];
                        self.selectedSound = self.sounds[0];
                        [self playSound];
                        self.imagenesClasificadas++;
                    }
                    else {
                       
                        [self regresate:draggedButton];
                        self.selectedSound = self.sounds[1];
                        [self playSound];
                        
                        
                    }
                    
                    
                    
                }
                
                 self.imgLabelClasificadas.text= [NSString stringWithFormat:@"Imágenes:%d/%d",self.imagenesClasificadas,self.totalElementos];
                
            }
            
            
        }
        
        
        
    }
    
    
    
    
}

-(BOOL) seEncontro: (UIButton*) draggedButton : (int)numerosRegistros : (NSMutableArray*) arreglo{
    
    for (int i=0; i< numerosRegistros; i++){
        
        if ([arreglo[i][1] isEqualToString:draggedButton.currentTitle]){
           
            return YES;
        }
       
        
    }
    return NO;
}



-(void) desapacere:(UIButton*) boton1 {
    
    
    [UIView transitionWithView: boton1 duration:1.0 options: UIViewAnimationOptionTransitionCrossDissolve
     
                    animations:^{ boton1.hidden = YES; }
     
                    completion:^(BOOL finished){
                        
                        if(finished)
                            
                            [boton1 removeFromSuperview];
                    }];
    
}


-(void) regresate:(UIButton*) boton1 {
    
    int posX = 510;
    int posY = 610;
    
    [UIView transitionWithView: boton1 duration:1.0 options: UIViewAnimationOptionTransitionCrossDissolve
     
                    animations:^{
                                              
                        [UIView animateWithDuration:0.5
                                              delay:0.0
                                            options:UIViewAnimationOptionCurveEaseInOut
                                         animations:^{
                                            

                                             boton1.center = CGPointMake(posX,posY);
                                         }
                                         completion:nil];
                    }
     
     
                    completion:nil];
    
}

-(NSString *) getNowToString{
    NSDate *now = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-6]];
  
    return [formatter stringFromDate:now];
}


@end
