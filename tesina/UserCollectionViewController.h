//
//  UserCollectionViewController.h
//  tesina
//
//  Created by Nancy on 02/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "DBManager.h"
#import "CategoriaCollectionViewController.h"
@interface UserCollectionViewController : UICollectionViewController

@property (strong,nonatomic) NSMutableArray *nombresObtenidos;

@end
