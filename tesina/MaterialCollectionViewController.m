//
//  MaterialCollectionViewController.m
//  tesina
//
//  Created by Nancy on 12/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import "MaterialCollectionViewController.h"

@interface MaterialCollectionViewController ()

@end

@implementation MaterialCollectionViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"Categoria:%@",self.idCategoria);
    
      [self recuperarNombres];
}

//metodo que se realiza cuando aparece en escena
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self recuperarNombres];
    [self.collectionView reloadData];
}


-(void) recuperarNombres{
    
    DBManager *dbManager = [[DBManager alloc] init];
    
    //parseando texto
    NSString *query= [NSString stringWithFormat:@"Select m.id,m.nombre from material m, categoria_material cm where m.id=cm.id_material and cm.id_categoria =%@",self.idCategoria];

    
    NSMutableArray *matriz = [dbManager obtenerConsulta:query];
    
    self.imagenesObtenidas=matriz;
    
    int numeroRegistros = (int)matriz.count;
    
    for (int i=0; i<numeroRegistros; i++){
        
        
        
        for (int j=0; j<2; j++){
            
            NSLog(@"%@", self.imagenesObtenidas[i][j]);
            
            
        }
    }
    
    
    
}




- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //regresar el numero de items
    
    return self.imagenesObtenidas.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    //automaticamente se hace un ciclo con respecto al número de items
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    //obteniendo el string adecuado
    NSString *texto = self.imagenesObtenidas[indexPath.row][1];
    
    //parseando texto
    NSString *textoString= [NSString stringWithFormat:@"%@",texto];
    
    //obteniendo paths
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    //obteniendo path destino con el nombre de la imagen
    NSString *toPath = [NSString stringWithFormat:@"%@/materiales/%@.png",documentDirectory,textoString];
    
    
    //variable para validar si el archivo existe
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:toPath];
    
    
    if (fileExists == YES){
        
        //Buscando el label atraves del tag
        UILabel *labelName = (UILabel *)[cell viewWithTag:2];
        //BUscando la imagen a través del tag
        UIImageView *imagen = (UIImageView *)[cell viewWithTag:1];
        
        //guardando el texto
        labelName.text = textoString;
        
        //obteniendo imagen
        UIImage *imagenObtenida = [UIImage imageWithContentsOfFile:toPath];
        //asignando imagen
        imagen.image = imagenObtenida;
        
        
    }
    else {
        
        NSLog(@"Error");
    }
    

    
    return cell;
}



-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if( ![segue.identifier isEqualToString:@"anadirMaterialSegue"]){
        
        //obteniendo arreglo
        NSArray *elementos= [self.collectionView indexPathsForSelectedItems];
        //obteniendo valor del index del seleccionado
        NSInteger index=((NSIndexPath *)elementos[0]).row;
        
        
        //parseando el string del seleccionado
        NSMutableArray *seleccionado= self.imagenesObtenidas[index];
        
        if([segue.identifier isEqualToString:@"modificarMaterialSegue"]){
            
            MaterialCrudViewController *controller=(MaterialCrudViewController *) segue.destinationViewController;
            
            
            //pasando la variable a la siguiente vista
            controller.idCategoria = self.idCategoria;
            controller.material = seleccionado;
        }
        
        
        
    }
    else {
    
        MaterialCrudViewController *controller=(MaterialCrudViewController *) segue.destinationViewController;
        
        
        //pasando la variable a la siguiente vista
        controller.idCategoria = self.idCategoria;
        
    }
    
}


@end
