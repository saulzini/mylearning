//
//  ReportesTableViewController.m
//  tesina
//
//  Created by Nancy on 14/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import "ReportesTableViewController.h"

@interface ReportesTableViewController ()

@end

@implementation ReportesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self recuperarNombres];
}




-(void) recuperarNombres{
    
    DBManager *dbManager = [[DBManager alloc] init];
    
    NSMutableArray *matriz = [dbManager obtenerConsulta:@"select u.nombre , c.nombre , j.puntaje,j.tiempo_realizado, j.fecha_juego  from jugar j , categoria c, usuario u where j.id_categoria = c.id and u.id = j.id_usuario"];
    
    self.registrosObtenidos=matriz;
    
    int numeroRegistros = (int)matriz.count;
    
    for (int i=0; i<numeroRegistros; i++){
        
        
        
        for (int j=0; j<2; j++){
            
            NSLog(@"%@", self.registrosObtenidos[i][j]);
            
            
        }
    }
    
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.registrosObtenidos.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    //Buscando el label atraves del tag
    UILabel *labelName = (UILabel *)[cell viewWithTag:1];
    //Buscando el label atraves del tag
    UILabel *categoriaName = (UILabel *)[cell viewWithTag:2];
    //Buscando el label atraves del tag
    UILabel *puntajeName = (UILabel *)[cell viewWithTag:3];
    //Buscando el label atraves del tag
    UILabel *tiempoName = (UILabel *)[cell viewWithTag:4];
    //Buscando el label atraves del tag
    UILabel *fechaName = (UILabel *)[cell viewWithTag:5];
    
    //obteniendo el string adecuado solo la segunda columna son nombres
    NSString *usuario =self.registrosObtenidos[indexPath.row][0];
    
    //obteniendo el string adecuado solo la segunda columna son nombres
    NSString *categoria =self.registrosObtenidos[indexPath.row][1];
    
    //obteniendo el string adecuado solo la segunda columna son nombres
    NSString *puntaje =self.registrosObtenidos[indexPath.row][2];
    
    //obteniendo el string adecuado solo la segunda columna son nombres
    NSString *tiempo =self.registrosObtenidos[indexPath.row][3];
    
    //obteniendo el string adecuado solo la segunda columna son nombres
    NSString *fecha =self.registrosObtenidos[indexPath.row][4];
    
    
    //parseando texto
    NSString *usuarioString= [NSString stringWithFormat:@"Usuario:%@",usuario];
    //parseando texto
    NSString *categoriaString= [NSString stringWithFormat:@"Categoria:%@",categoria];
    
    //parseando texto
    NSString *puntajeString= [NSString stringWithFormat:@"Puntaje:%@",puntaje];
    
    //parseando texto
    NSString *tiempoString= [NSString stringWithFormat:@"Tiempo:%@",tiempo];
    
    NSArray *nuevaFecha=[fecha componentsSeparatedByString:@","];
    //parseando texto
    NSString *fechaString= [NSString stringWithFormat:@"Fecha:%@",nuevaFecha[0]];
    
    
    //guardando el texto
    labelName.text = usuarioString;
    categoriaName.text = categoriaString;
    puntajeName.text = puntajeString;
    tiempoName.text = tiempoString;
    fechaName.text = fechaString;
    
    
    
    return cell;
}


@end
