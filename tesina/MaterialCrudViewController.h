//
//  MaterialCrudViewController.h
//  tesina
//
//  Created by Nancy on 12/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
@interface MaterialCrudViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *NombreTextField;
@property (weak, nonatomic) IBOutlet UIButton *posterBtn;

@property (nonatomic, assign) UIImage* imagenSeleccionada;
@property (strong,nonatomic) NSMutableArray *material;
@property (strong,nonatomic) NSString *idCategoria;


@property (nonatomic, assign) BOOL anadir;
@end
