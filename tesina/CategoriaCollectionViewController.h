//
//  CategoriaCollectionViewController.h
//  tesina
//
//  Created by Nancy on 03/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "CategoriaCrudViewController.h"
#import "MaterialCollectionViewController.h"
#import "JuegoViewController.h"
@interface CategoriaCollectionViewController : UICollectionViewController


@property (strong,nonatomic) NSMutableArray *usuarioSeleccionado;

@property (strong,nonatomic) NSMutableArray *imagenesObtenidas;

@end
