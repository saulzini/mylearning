//
//  AppDelegate.h
//  tesina
//
//  Created by Nancy on 19/02/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

