//
//  MainViewController.m
//  tesina
//
//  Created by Nancy on 09/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     DBManager *dbManager = [[DBManager alloc] init];
    [dbManager createEditableCopyOfDatabaseIfNeeded ];
    
    //creando directorios para guardar las imagenes
    [self createDirectory:@"categorias"];
    [self createDirectory:@"materiales"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//metodo para crear directorios
-(void)createDirectory:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSError *error;
    if (![[NSFileManager defaultManager] createDirectoryAtPath:[documentsDirectory stringByAppendingPathComponent:name]
                                   withIntermediateDirectories:NO
                                                    attributes:nil
                                                         error:&error])
    {
        NSLog(@"Create directory error: %@", error);
    }
}


@end
