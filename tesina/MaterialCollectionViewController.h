//
//  MaterialCollectionViewController.h
//  tesina
//
//  Created by Nancy on 12/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "MaterialCrudViewController.h"

@interface MaterialCollectionViewController : UICollectionViewController

@property (nonatomic, assign) NSString* idCategoria;

@property (strong,nonatomic) NSMutableArray *imagenesObtenidas;

@end
