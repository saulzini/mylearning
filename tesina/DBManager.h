//
//  NSObject+DBManager.h
//  tesina
//
//  Created by Nancy on 05/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@interface DBManager:NSObject

-(NSString *) getWritableDBPath ;

- (NSMutableArray*)obtenerConsulta:(NSString *)query;

- (BOOL)DML:(NSString *)query;
-(void)createEditableCopyOfDatabaseIfNeeded;

@end
