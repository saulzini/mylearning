//
//  CategoriaCrudViewController.m
//  tesina
//
//  Created by Nancy on 09/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import "CategoriaCrudViewController.h"

@interface CategoriaCrudViewController ()

@end

@implementation CategoriaCrudViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.anadir = YES;
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
   
    if (self.categoria != nil || [self.categoria count] > 0){
    
        self.anadir = NO;
        self.navigationItem.rightBarButtonItem.enabled = YES;

        
        NSString *nombre = self.categoria[1];
    
        //obteniendo paths
        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDirectory = [paths objectAtIndex:0];
        
        
        //obteniendo path destino con el nombre de la imagen
        NSString *toPath = [NSString stringWithFormat:@"%@/categorias/%@.png",documentDirectory,
                            nombre];
        
        //obteniendo imagen
        UIImage *imagenObtenida = [UIImage imageWithContentsOfFile:toPath];
        self.imagenSeleccionada = imagenObtenida;
        self.NombreTextField.text =nombre ;
        
        [self.posterBtn setBackgroundImage:imagenObtenida forState:UIControlStateNormal];
        
        
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)selectedMoviePoster:(UIButton *)sender {
    
    
    //Creando mensaje de confirmación
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Imagen"
                                 message:@"Selecciona una opción"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    // boton de accion
    UIAlertAction* libreria = [UIAlertAction
                                actionWithTitle:@"Librería"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    
                                    //si presiona libreria
                                    NSLog(@"Selecciono libreria");
                                    [self selectImageFromImagePicker];
                                    
                                }];
    // boton de accion
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //si presiona cancelar
                               }];
    
    [alert addAction:libreria];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)selectImageFromImagePicker {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
}




-(void)imagePickerController:(UIImagePickerController
                              *)picker didFinishPickingMediaWithInfo:(NSDictionary
                                                                      *)info{
    
    UIImage *imagen =  [info
                             objectForKey:UIImagePickerControllerOriginalImage];
    if (imagen){
        [self.posterBtn setBackgroundImage:imagen forState:UIControlStateNormal];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        self.imagenSeleccionada = imagen;
        
    }
}

- (IBAction)accionGuardar:(id)sender {
    
    
    if (![self.NombreTextField.text isEqual: @""] && self.imagenSeleccionada != nil){
        
        if (self.anadir ==YES){
            //obteniendo paths
            NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentDirectory = [paths objectAtIndex:0];
            
            //obteniendo path destino con el nombre de la imagen
            NSString *toPath = [NSString stringWithFormat:@"%@/categorias/%@.png",documentDirectory,self.NombreTextField.text];
            
            //variable para el error
            NSError *error;
            
            //variable para validar si el archivo existe
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:toPath];
            
            
            if (fileExists == NO){
                
                //copiando imagen a destino
                [UIImagePNGRepresentation(self.imagenSeleccionada) writeToFile:toPath options:NSDataWritingAtomic error:&error];
                
                if (error){
                    NSLog(@"%@",error);
                }
                else {
                    DBManager *dbManager = [[DBManager alloc] init];
                    
                    
                    NSString *query = [NSString stringWithFormat: @"Insert into categoria (nombre,imagen) values ('%@','%@')",self.NombreTextField.text,self.NombreTextField.text];
                    
                    
                    BOOL aceptado = [dbManager DML: query];
                    
                    if (aceptado){
                        NSLog(@"Se ha guardado la categoría");
                        //Se cierra la ventana actual y se regresa una anterior
                        [self.navigationController popViewControllerAnimated:YES];
                        
                    }
                    else {
                        NSLog(@"Error");
                    }
                    
                    
                }
            }
            else {
                //Creando mensaje de error de nombre repetido
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Error"
                                             message:@"Nombre existente favor de elegir otro"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                // boton de accion
                UIAlertAction* aceptar = [UIAlertAction
                                          actionWithTitle:@"Aceptar"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action) {
                                              //si presiona cancelar
                                          }];
                
                [alert addAction:aceptar];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
        }
        else {
            //modificar
            //Creando mensaje de confirmación
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Modificar"
                                         message:@"¿Está seguro que desea modificar el registro?"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //boton de accion
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Modificar"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            
                                            //modificar
                                
                                            //si el nombre es igual entonces puedes modificar solo la imagen
                                            if ([self.NombreTextField.text isEqualToString:self.categoria[1]] ){
                                                NSLog(@"Son iguales");
                                                [self modificar];
                                                
                                            }else {
                                            
                                                
                                                //obteniendo paths
                                                NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
                                                NSString *documentDirectory = [paths objectAtIndex:0];
                                                
                                                //obteniendo path destino con el nombre de la imagen
                                                NSString *toPath = [NSString stringWithFormat:@"%@/categorias/%@.png",documentDirectory,self.NombreTextField.text];
                                                
                                                //variable para el error
                                                NSError *error;
                                                
                                                //variable para validar si el archivo existe
                                                BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:toPath];
                                                
                                                
                                                if (fileExists == NO){
                                                    
                                                    //copiando imagen a destino
                                                    [UIImagePNGRepresentation(self.imagenSeleccionada) writeToFile:toPath options:NSDataWritingAtomic error:&error];
                                                    
                                                    //obteniendo path destino con el nombre de la imagen antiguo para borrar
                                                    NSString *toPathAntiguo = [NSString stringWithFormat:@"%@/categorias/%@.png",documentDirectory,self.categoria[1] ];
                                                    //eliminando imagen
                                                    [[NSFileManager defaultManager] removeItemAtPath:toPathAntiguo error: &error];
                                                    
                                                    
                                                    if (error){
                                                        NSLog(@"%@",error);
                                                    }
                                                    else {
                                                        DBManager *dbManager = [[DBManager alloc] init];
                                                        
                                                        NSString *query = [NSString stringWithFormat: @"Update categoria set nombre='%@', imagen='%@' where id=%@",self.NombreTextField.text,self.NombreTextField.text,self.categoria[0]];
                                                        
                                                      
                                                        
                                                        BOOL aceptado = [dbManager DML: query];
                                                        
                                                        if (aceptado){
                                                            NSLog(@"Se ha guardado la categoría");
                                                            //Se cierra la ventana actual y se regresa una anterior
                                                            [self.navigationController popViewControllerAnimated:YES];
                                                            
                                                        }
                                                        else {
                                                            NSLog(@"Error");
                                                        }
                                                        
                                                        
                                                        
                                                    }
                                                }
                                                else {
                                                    //Creando mensaje de error de nombre repetido
                                                    
                                                    UIAlertController * alert = [UIAlertController
                                                                                 alertControllerWithTitle:@"Error"
                                                                                 message:@"Nombre existente favor de elegir otro"
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                                    
                                                    // boton de accion
                                                    UIAlertAction* aceptar = [UIAlertAction
                                                                              actionWithTitle:@"Aceptar"
                                                                              style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action) {
                                                                                  //si presiona cancelar
                                                                              }];
                                                    
                                                    [alert addAction:aceptar];
                                                    [self presentViewController:alert animated:YES completion:nil];
                                                    
                                                }
                                            }
                                            
                                            
                                            
                                        }];
            //boton de accion
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Cancelar"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //si presiona cancelar
                                       }];
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }
    else {
    
        //Creando mensaje de error de nombre repetido
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Error"
                                     message:@"Favor de escribir un nombre y elegir una imagen"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        // boton de accion
        UIAlertAction* aceptar = [UIAlertAction
                                  actionWithTitle:@"Aceptar"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                      //si presiona cancelar
                                  }];
        
        [alert addAction:aceptar];
        [self presentViewController:alert animated:YES completion:nil];

    
    }
   
}

- (void)modificar {
    DBManager *dbManager = [[DBManager alloc] init];
    
    
    //obteniendo paths
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
   
    
    //obteniendo path destino con el nombre de la imagen
    NSString *toPath = [NSString stringWithFormat:@"%@/categorias/%@.png",documentDirectory,self.NombreTextField.text];
    
    //variable para el error
    NSError *error;

    
    //copiando imagen a destino
    [UIImagePNGRepresentation(self.imagenSeleccionada) writeToFile:toPath options:NSDataWritingAtomic error:&error];
    
    NSString *query = [NSString stringWithFormat: @"Update categoria set nombre='%@', imagen='%@' where id=%@",self.NombreTextField.text,self.NombreTextField.text,self.categoria[0]];
   
    
    BOOL aceptado = [dbManager DML: query];
    
    if (aceptado){
        NSLog(@"Se ha guardado la categoría");
        //Se cierra la ventana actual y se regresa una anterior
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else {
        NSLog(@"Error");
    }
    

}
- (IBAction)btnBorrar:(id)sender {
    
        
    //Creando mensaje de confirmación
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Eliminar"
                                 message:@"¿Está seguro que desea eliminar el registro?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    // boton de accion
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Eliminar"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    
                                    //si presiona eliminar
                                    
                                    DBManager *dbManager = [[DBManager alloc] init];
                                    
                                    
                                    NSString *queryR = [NSString stringWithFormat: @"Delete from categoria_material where id_categoria=%@",self.categoria[0]];
                                    
                                    
                                    BOOL aceptadoR = [dbManager DML: queryR];
                                    
                                    
                                    NSString *queryT = [NSString stringWithFormat: @"Delete from categoria where id=%@",self.categoria[0]];
                                    
                                    
                                    BOOL aceptadoT = [dbManager DML: queryT];
                                    
                                    
                                    //obteniendo paths
                                    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
                                    NSString *documentDirectory = [paths objectAtIndex:0];
                                    
                                    //obteniendo path destino con el nombre de la imagen
                                    NSString *toPath = [NSString stringWithFormat:@"%@/categorias/%@.png",documentDirectory,self.NombreTextField.text];
                                    
                                    //variable para el error
                                    NSError *error;

                                    [[NSFileManager defaultManager] removeItemAtPath:toPath error: &error];
                                    
                                    if (aceptadoT && aceptadoR){
                                        NSLog(@"Se ha eliminado");
                                        //Se cierra la ventana actual y se regresa una anterior
                                        [self.navigationController popViewControllerAnimated:YES];
                                        
                                    }
                                    else {
                                        NSLog(@"Error");
                                    }
                                    
                                    
                                    
                                }];
    // boton de accion
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //si presiona cancelar
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    

    
}

@end
