//
//  CategoriaCrudViewController.h
//  tesina
//
//  Created by Nancy on 09/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import "ViewController.h"


@interface CategoriaCrudViewController : ViewController
@property (weak, nonatomic) IBOutlet UITextField *NombreTextField;
@property (weak, nonatomic) IBOutlet UIButton *posterBtn;
@property (weak, nonatomic) IBOutlet UIButton *guardarBtn;

@property (nonatomic, assign) UIImage* imagenSeleccionada;


@property (strong,nonatomic) NSMutableArray *categoria;

@property (nonatomic, assign) BOOL anadir;


@end
