//
//  UserCollectionViewController.m
//  tesina
//
//  Created by Nancy on 02/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import "UserCollectionViewController.h"
#import "CategoriaCollectionViewController.h"
#import "ViewController.h"

@interface UserCollectionViewController ()
@end

//NSString * myDB=@"basededatos.db";


@implementation UserCollectionViewController


- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self recuperarNombres];
    
    
    
    
}


//metodo que se realiza cuando aparece en escena
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self recuperarNombres];
    [self.collectionView reloadData];
}


-(void) recuperarNombres{
    
    DBManager *dbManager = [[DBManager alloc] init];
    
    NSMutableArray *matriz = [dbManager obtenerConsulta:@"Select id,nombre from usuario"];
    
    self.nombresObtenidos=matriz;
    
    int numeroRegistros = (int)matriz.count;
    
    for (int i=0; i<numeroRegistros; i++){
    
        
        
        for (int j=0; j<2; j++){

            NSLog(@"%@", self.nombresObtenidos[i][j]);
          
            
        }
    }
    
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//metodo para regresar el numero de items
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    //regresar el numero de items
    
    return self.nombresObtenidos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //automaticamente se hace un ciclo con respecto al número de items
    
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    //Buscando el label atraves del tag
    UILabel *labelName = (UILabel *)[cell viewWithTag:1];
    
    //obteniendo el string adecuado solo la segunda columna son nombres
    NSString *texto =self.nombresObtenidos[indexPath.row][1];
    
    //parseando texto
    NSString *textoString= [NSString stringWithFormat:@"%@",texto];

    //guardando el texto
    labelName.text = textoString;

    //colorear bordes
    cell.layer.borderWidth=1.0f;
    cell.layer.borderColor=[UIColor blueColor].CGColor;
    
    return cell;
}




-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if( ![segue.identifier isEqualToString:@"usuarioAnadirSegue"]){
        //obteniendo arreglo
        NSArray *usuarios= [self.collectionView indexPathsForSelectedItems];
        //obteniendo valor del index del seleccionado
        NSInteger index=((NSIndexPath *)usuarios[0]).row;
        
        //parseando el string del seleccionado
        NSMutableArray *seleccionado= self.nombresObtenidos[index];
        
        if([segue.identifier isEqualToString:@"categoriaSegue"]){
            CategoriaCollectionViewController *categoriaController=(CategoriaCollectionViewController *) segue.destinationViewController;
            
            
            //pasando la variable a la siguiente vista
            categoriaController.usuarioSeleccionado = seleccionado;
            
        }
        
        //segue para modificar
        if([segue.identifier isEqualToString:@"usuarioModificarSegue"]){
            ViewController *controller=(ViewController *) segue.destinationViewController;
            
            
            
            //pasando la variable a la siguiente vista
            controller.usuario = seleccionado;
        }

    }
    
}



@end
