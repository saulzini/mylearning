//
//  NSObject+DBManager.m
//  tesina
//
//  Created by Nancy on 05/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import "DBManager.h"

@implementation DBManager

NSString * myDB=@"basededatos.db";

//========================================WRITABLE DATABASE PATH=======================
- (NSString *) getWritableDBPath {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:myDB];
    
}


- (NSMutableArray*)obtenerConsulta:(NSString *)query {
    
    NSLog(@"obtenerConsulta %@",query);
    //========================VIEW=========================
    
  
    NSString * paths=[self getWritableDBPath];
    
    const char *dbpath =  [paths UTF8String];
    sqlite3_stmt *statement=nil;
    static sqlite3 *database = nil;
    
    
    NSMutableArray *matriz=[[NSMutableArray alloc] init];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"%@", query];
        
        const char *query_stmt = [querySQL UTF8String];
        
        
        

        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            

            int columnCount = sqlite3_column_count(statement);

            while(sqlite3_step(statement) == SQLITE_ROW)
            {
            
                NSMutableArray *fila=[[NSMutableArray alloc] init];
               
                for(int i=0;i<columnCount;i++){
                    
                    NSString *stringObtenido = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, i)];
                    
                    
                    [fila addObject:stringObtenido];
                   /*
                    //obteniendo chars de la columna
                    const char *columnName=sqlite3_column_name(statement, i);
                    //convirtiendo a string
                    NSString *columnNameString=[[NSString alloc] initWithUTF8String:columnName];

                    NSLog(@"%@: %@", columnNameString,stringObtenido);
                    */
                }
                
                
                [matriz addObject:fila];

                
            }
            NSLog(@"Termino");
            sqlite3_finalize(statement);
        }
        else {
            NSLog( @"Error '%s'", sqlite3_errmsg(database));
        }
        
        
        
        sqlite3_close(database);
    }
    
    return matriz;
    
}




-(BOOL)DML:(NSString *)query { //========================SAVE==============================
    

    // Copy the database if needed
    
    [self createEditableCopyOfDatabaseIfNeeded];
    
    NSString *filePath = [self getWritableDBPath];
    sqlite3 *database;

    
    if(sqlite3_open([filePath UTF8String], &database) == SQLITE_OK) {
        
        
        NSString *querySQL = [NSString stringWithFormat: @"%@", query];

        const char *query_stmt = [querySQL UTF8String];

        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, query_stmt, -1, &compiledStatement, NULL) == SQLITE_OK)  {
            
            
            if (sqlite3_step(compiledStatement) == SQLITE_DONE)
            {
                return YES;
            }
            else {
                return NO;
            }
            
        }
        if(sqlite3_step(compiledStatement) != SQLITE_DONE ) {
            NSLog( @"Error: %s", sqlite3_errmsg(database) );
        }
        sqlite3_finalize(compiledStatement);
        sqlite3_close(database);
    }
    return NO;
    
}

//================================================Copies database to appropriate location============

-(void)createEditableCopyOfDatabaseIfNeeded{
    // Testing for existence
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:myDB];
    
    success = [fileManager fileExistsAtPath:writableDBPath];
    
    
    
    if (success)
        return;
    
    // The writable database does not exist, so copy the default to
    // the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath]
                               stringByAppendingPathComponent:myDB];
    success = [fileManager copyItemAtPath:defaultDBPath
                                   toPath:writableDBPath
                                    error:&error];
  
    
    if(!success)
    {
        NSAssert1(0,@"Failed to create writable database file with Message : '%@'.",
                  [error localizedDescription]);
    }
}


@end
