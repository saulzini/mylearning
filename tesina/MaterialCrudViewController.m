//
//  MaterialCrudViewController.m
//  tesina
//
//  Created by Nancy on 12/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import "MaterialCrudViewController.h"

@interface MaterialCrudViewController ()

@end

@implementation MaterialCrudViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"Categoria seleccionada: %@", self.idCategoria);
    NSLog(@"%@", self.material);
    
    
    self.anadir=YES;
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    //validar arreglo para saber si dio click en anadir
    //Anadir es igual a falso
    if (self.material != nil || [self.material count] > 0)
    {
        self.anadir = NO;
        self.navigationItem.rightBarButtonItem.enabled = YES;
        
        NSString *nombre = self.material[1];
        
        //obteniendo paths
        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDirectory = [paths objectAtIndex:0];
        
        
        //obteniendo path destino con el nombre de la imagen
        NSString *toPath = [NSString stringWithFormat:@"%@/materiales/%@.png",documentDirectory,
                            nombre];
        
        //obteniendo imagen
        UIImage *imagenObtenida = [UIImage imageWithContentsOfFile:toPath];
        self.imagenSeleccionada = imagenObtenida;
        self.NombreTextField.text =nombre ;
        
        [self.posterBtn setBackgroundImage:imagenObtenida forState:UIControlStateNormal];
        
   
    
    
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnImagen:(id)sender {
    
    //Creando mensaje de confirmación
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Imagen"
                                 message:@"Selecciona una opción"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    // boton de accion
    UIAlertAction* libreria = [UIAlertAction
                               actionWithTitle:@"Librería"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   
                                   //si presiona libreria
                                   NSLog(@"Selecciono libreria");
                                   [self selectImageFromImagePicker];
                                   
                               }];
    // boton de accion
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //si presiona cancelar
                               }];
    
    [alert addAction:libreria];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


-(void)selectImageFromImagePicker {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
}




-(void)imagePickerController:(UIImagePickerController
                              *)picker didFinishPickingMediaWithInfo:(NSDictionary
                                                                      *)info{
    
    UIImage *imagen =  [info
                        objectForKey:UIImagePickerControllerOriginalImage];
    if (imagen){
        [self.posterBtn setBackgroundImage:imagen forState:UIControlStateNormal];
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        self.imagenSeleccionada = imagen;
        
    }
}



- (IBAction)btnGuardar:(id)sender {
    
    if (![self.NombreTextField.text isEqual: @""] && self.imagenSeleccionada != nil){
        
        if (self.anadir ==YES){
            //obteniendo paths
            NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentDirectory = [paths objectAtIndex:0];
            
            //obteniendo path destino con el nombre de la imagen
            NSString *toPath = [NSString stringWithFormat:@"%@/materiales/%@.png",documentDirectory,self.NombreTextField.text];
            
            //variable para el error
            NSError *error;
            
            //variable para validar si el archivo existe
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:toPath];
            
            
            if (fileExists == NO){
                
                //copiando imagen a destino
                [UIImagePNGRepresentation(self.imagenSeleccionada) writeToFile:toPath options:NSDataWritingAtomic error:&error];
                
                if (error){
                    NSLog(@"%@",error);
                }
                else {
                    DBManager *dbManager = [[DBManager alloc] init];
                    
                    
                    NSString *query = [NSString stringWithFormat: @"Insert into material (nombre,imagen) values ('%@','%@')",self.NombreTextField.text,self.NombreTextField.text];
                    
                    BOOL aceptado = [dbManager DML: query];
                    
                    //Obteniendo el ultimo agregado
                     NSString *queryUltimoAgregado = [NSString stringWithFormat: @" select seq from sqlite_sequence where name='material'"];
                   
                     NSMutableArray *ultimoID = [dbManager obtenerConsulta: queryUltimoAgregado];
                    
                    
                    //Relacion
                    
                     NSString *queryRelacion = [NSString stringWithFormat: @"Insert into categoria_material (id_material,id_categoria) values ('%@','%@')",
                                        ultimoID[0][0],self.idCategoria];
                    
                    
                    BOOL aceptadoR = [dbManager DML: queryRelacion];
                    
                    
                    if (aceptado && aceptadoR){
                        NSLog(@"Se ha guardado el material");
                        //Se cierra la ventana actual y se regresa una anterior
                        [self.navigationController popViewControllerAnimated:YES];
                        
                    }
                    else {
                        NSLog(@"Error");
                    }
                    
                    
                }
            }
            else {
                //Creando mensaje de error de nombre repetido
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Error"
                                             message:@"Nombre existente favor de elegir otro"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                // boton de accion
                UIAlertAction* aceptar = [UIAlertAction
                                          actionWithTitle:@"Aceptar"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action) {
                                              //si presiona cancelar
                                          }];
                
                [alert addAction:aceptar];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
        }
        else {
            //modificar
            //Creando mensaje de confirmación
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Modificar"
                                         message:@"¿Está seguro que desea modificar el registro?"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //boton de accion
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Modificar"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            
                                             //modificar
                                            
                                            //si el nombre es igual entonces puedes modificar solo la imagen
                                            if ([self.NombreTextField.text isEqualToString:self.material[1]] ){
                                                
                                                [self modificar];
                                                
                                            }else {
                                                
                                                
                                                //obteniendo paths
                                                NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
                                                NSString *documentDirectory = [paths objectAtIndex:0];
                                                
                                                
                                                
                                                //obteniendo path destino con el nombre de la imagen
                                                NSString *toPath = [NSString stringWithFormat:@"%@/materiales/%@.png",documentDirectory,self.NombreTextField.text];
                                                
                                                //variable para el error
                                                NSError *error;
                                                
                                                //variable para validar si el archivo existe
                                                BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:toPath];
                                                
                                                
                                                if (fileExists == NO){
                                                    
                                    
                                                    
                                                    //copiando imagen a destino
                                                    [UIImagePNGRepresentation(self.imagenSeleccionada) writeToFile:toPath options:NSDataWritingAtomic error:&error];
                                                    
                                                    //obteniendo path destino con el nombre de la imagen antiguo para borrar
                                                    NSString *toPathAntiguo = [NSString stringWithFormat:@"%@/materiales/%@.png",documentDirectory,self.material[1] ];
                                                    //eliminando imagen
                                                    [[NSFileManager defaultManager] removeItemAtPath:toPathAntiguo error: &error];
                                                    
                                                    if (error){
                                                        NSLog(@"%@",error);
                                                    }
                                                    else {
                                                        DBManager *dbManager = [[DBManager alloc] init];
                                                        
                                                        NSString *query = [NSString stringWithFormat: @"Update material set nombre='%@', imagen='%@' where id=%@",self.NombreTextField.text,self.NombreTextField.text,self.material[0]];
                                                        
                                                        NSLog(@"Query: %@",query);
                                                        
                                                        BOOL aceptado = [dbManager DML: query];
                                                        
                                                        if (aceptado){
                                                            NSLog(@"Se ha guardado el material");
                                                            //Se cierra la ventana actual y se regresa una anterior
                                                            [self.navigationController popViewControllerAnimated:YES];
                                                            
                                                        }
                                                        else {
                                                            NSLog(@"Error");
                                                        }
                                                        
                                                        
                                                        
                                                    }
                                                }
                                                else {
                                                    //Creando mensaje de error de nombre repetido
                                                    
                                                    UIAlertController * alert = [UIAlertController
                                                                                 alertControllerWithTitle:@"Error"
                                                                                 message:@"Nombre existente favor de elegir otro"
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                                    
                                                    // boton de accion
                                                    UIAlertAction* aceptar = [UIAlertAction
                                                                              actionWithTitle:@"Aceptar"
                                                                              style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action) {
                                                                                  //si presiona cancelar
                                                                              }];
                                                    
                                                    [alert addAction:aceptar];
                                                    [self presentViewController:alert animated:YES completion:nil];
                                                    
                                                }
                                            }
                                            
                                            
                                            
                                        }];
            //boton de accion
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Cancelar"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //si presiona cancelar
                                       }];
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }
    else {
        
        //Creando mensaje de error de nombre repetido
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Error"
                                     message:@"Favor de escribir un nombre y elegir una imagen"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        // boton de accion
        UIAlertAction* aceptar = [UIAlertAction
                                  actionWithTitle:@"Aceptar"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                      //si presiona cancelar
                                  }];
        
        [alert addAction:aceptar];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }

    
}



- (void)modificar {
    DBManager *dbManager = [[DBManager alloc] init];
    
    
    
    //obteniendo paths
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
   
    
    //obteniendo path destino con el nombre de la imagen
    NSString *toPath = [NSString stringWithFormat:@"%@/materiales/%@.png",documentDirectory,self.NombreTextField.text];
    
    
    //variable para el error
    NSError *error;
    
    
    
    //copiando imagen a destino
    [UIImagePNGRepresentation(self.imagenSeleccionada) writeToFile:toPath options:NSDataWritingAtomic error:&error];
    
    NSString *query = [NSString stringWithFormat: @"Update material set nombre='%@', imagen='%@' where id=%@",self.NombreTextField.text,self.NombreTextField.text,self.material[0]];
    
    NSLog(@"Query: %@",query);
    
    BOOL aceptado = [dbManager DML: query];
    
    if (aceptado){
        NSLog(@"Se ha guardado el material");
        //Se cierra la ventana actual y se regresa una anterior
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else {
        NSLog(@"Error");
    }
    
    
}

- (IBAction)btnBorrar:(id)sender {
    
    
    //Creando mensaje de confirmación
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Eliminar"
                                 message:@"¿Está seguro que desea eliminar el registro?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    // boton de accion
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Eliminar"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    
                                    //si presiona eliminar
                                    
                                    DBManager *dbManager = [[DBManager alloc] init];
                                    
                                    
                                    NSString *queryR = [NSString stringWithFormat: @"Delete from categoria_material where id_material=%@",self.material[0]];
                                    
                                    
                                    BOOL aceptadoR = [dbManager DML: queryR];
                                    
                                    
                                    NSString *queryT = [NSString stringWithFormat: @"Delete from material where id=%@",self.material[0]];
                                    
                                    
                                    BOOL aceptadoT = [dbManager DML: queryT];
                                    
                                    
                                    //obteniendo paths
                                    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
                                    NSString *documentDirectory = [paths objectAtIndex:0];
                                    
                                    //obteniendo path destino con el nombre de la imagen
                                    NSString *toPath = [NSString stringWithFormat:@"%@/materiales/%@.png",documentDirectory,self.NombreTextField.text];
                                    
                                    //variable para el error
                                    NSError *error;
                                    
                                    [[NSFileManager defaultManager] removeItemAtPath:toPath error: &error];
                                    
                                    if (aceptadoT && aceptadoR){
                                        NSLog(@"Se ha eliminado");
                                        //Se cierra la ventana actual y se regresa una anterior
                                        [self.navigationController popViewControllerAnimated:YES];
                                        
                                    }
                                    else {
                                        NSLog(@"Error");
                                    }
                                    
                                    
                                    
                                }];
    // boton de accion
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //si presiona cancelar
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
    

}


@end
