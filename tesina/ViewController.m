//
//  ViewController.m
//  tesina
//
//  Created by Nancy on 19/02/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.anadir=YES;
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    //validar arreglo para saber si dio click en anadir
    //Anadir es igual a falso
    if (self.usuario != nil || [self.usuario count] > 0) 
    {
        self.anadir = NO;
        self.navigationItem.rightBarButtonItem.enabled = YES;
        
        //self.usuario es un arreglo que recibe dos valores el id en posicion 0 y el nombre en 1
        self.nombreTextField.text = [NSString stringWithFormat: @"%@", self.usuario[1]];
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnGuardar:(id)sender {
    
    
    if (![self.nombreTextField.text  isEqual: @""]){
        
        DBManager *dbManager = [[DBManager alloc] init];
        
        //valida si es cierto
        if (self.anadir){
            
            NSString *query = [NSString stringWithFormat: @"Insert into usuario (nombre) values ('%@')",self.nombreTextField.text];
            
            
            BOOL aceptado = [dbManager DML: query];
            
            if (aceptado){
                NSLog(@"Se ha guardadoo");
                //Se cierra la ventana actual y se regresa una anterior
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            else {
                NSLog(@"Error");
            }
            
        }
        else {
            //modificar
            
            //Creando mensaje de confirmación
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Modificar"
                                         message:@"¿Está seguro que desea modificar el registro?"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //boton de accion
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Modificar"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            
                                            //si presiona guardar
                                            NSString *query = [NSString stringWithFormat: @"Update usuario set nombre='%@' where id=%@",self.nombreTextField.text,self.usuario[0]];
                                            
                                            
                                            BOOL aceptado = [dbManager DML: query];
                                            
                                            if (aceptado){
                                                NSLog(@"Se ha guardadoo");
                                                //Se cierra la ventana actual y se regresa una anterior
                                                [self.navigationController popViewControllerAnimated:YES];
                                                
                                            }
                                            else {
                                                NSLog(@"Error");
                                            }
                                            
                                            
                                            
                                        }];
            //boton de accion
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Cancelar"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //si presiona cancelar
                                       }];
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
    }
    else {
    
        //Creando mensaje de error de nombre repetido
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Error"
                                     message:@"Favor de escribir un nombre"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        // boton de accion
        UIAlertAction* aceptar = [UIAlertAction
                                  actionWithTitle:@"Aceptar"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                      //si presiona cancelar
                                  }];
        
        [alert addAction:aceptar];
        [self presentViewController:alert animated:YES completion:nil];
    
    }
    
 
    
}

- (IBAction)btnEliminar:(id)sender {
    
    //Creando mensaje de confirmación

    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Eliminar"
                                 message:@"¿Está seguro que desea eliminar el registro?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    // boton de accion
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Eliminar"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    
                                    //si presiona eliminar
                                    
                                    DBManager *dbManager = [[DBManager alloc] init];
                                    
                                    
                                    NSString *query = [NSString stringWithFormat: @"Delete from usuario where id=%@",self.usuario[0]];
                                    
                                    
                                    BOOL aceptado = [dbManager DML: query];
                                    
                                    if (aceptado){
                                        NSLog(@"Se ha eliminado");
                                        //Se cierra la ventana actual y se regresa una anterior
                                        [self.navigationController popViewControllerAnimated:YES];
                                        
                                    }
                                    else {
                                        NSLog(@"Error");
                                    }
                                    
                                    
                                    
                                }];
     // boton de accion
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //si presiona cancelar
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
    
}

@end
