//
//  ViewController.h
//  tesina
//
//  Created by Nancy on 19/02/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "DBManager.h"
@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nombreTextField;
@property (weak, nonatomic) IBOutlet UILabel *nombreLabel;

@property (weak, nonatomic) IBOutlet UIButton *botonGuardar;

@property (strong,nonatomic) NSMutableArray *usuario;
@property (nonatomic, assign) BOOL anadir;

@end

