//
//  JuegoViewController.h
//  tesina
//
//  Created by Nancy on 13/03/17.
//  Copyright © 2017 Saul Juarez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AVFoundation/AVFoundation.h>
#import "DBManager.h"

@interface JuegoViewController : UIViewController


@property (strong,nonatomic) NSTimer *timer;

@property (weak, nonatomic) IBOutlet UILabel *labelTiempo;

@property (nonatomic) int counter;

@property (strong,nonatomic) NSMutableArray *imagenesObtenidas;
@property (strong,nonatomic) NSMutableArray *imagenesObtenidas2;


@property (strong,nonatomic) NSMutableArray *categoriasObtenidas;

@property (strong,nonatomic) NSMutableArray *sounds;
@property (strong,nonatomic) AVAudioPlayer *audioPlayer;
@property(strong,nonatomic)NSString *selectedSound;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *CategoryButtonsCollection;



@property (strong,nonatomic) NSString *idUsuario;
@property (strong,nonatomic) NSString *idCategoria1;
@property (strong,nonatomic) NSString *nombreCategoria1;


@property (strong,nonatomic) NSMutableArray *BotonesCategoria1;
@property (strong,nonatomic) NSMutableArray *BotonesCategoria2;

@property (nonatomic) int totalElementos;
@property (nonatomic) int imagenesClasificadas;

@property (nonatomic) int numeroRegistros;

@property (nonatomic) int numeroRegistros2;
@property (weak, nonatomic) IBOutlet UILabel *imgLabelClasificadas;

@end
